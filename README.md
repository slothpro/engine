[![pipeline status](https://gitlab.com/slothpro/engine/badges/master/pipeline.svg)](https://gitlab.com/slothpro/engine/commits/master)

# Попытка структоризации web проектов на GO в более чистом виде, чем обычно

## Как запустить:

`go get gitlab.com/slothpro/engine`

в папке `$GOPATH/src/gitlab.com/slothpro/engine запустить

`docker-compose up -d`

`go run /cmd/app/app.go`

в корне проекта лежит файл app.yml - это конфиг проекта

