package main

import (
	"fmt"
	"github.com/spf13/viper"
	"gitlab.com/slothpro/engine/domains/blog"
	"gitlab.com/slothpro/engine/domains/user"
	"gitlab.com/slothpro/engine/infrastructure/database/mysql"
	"gitlab.com/slothpro/engine/infrastructure/server"
)

type App struct {
	*server.Server
	*mysql.Mysql
	Config Config
}

type Config struct {
	DB struct {
		Login string
		Pass  string
		Host  string
		Port  int
		Name  string
	}
	Server struct {
		Ip   string
		Port int
	}
}

var app App

func init() {
	viper.AddConfigPath(".")
	viper.SetConfigName("app")

	viper.SetDefault("server.ip", "127.0.0.1")
	viper.SetDefault("server.Port", 8080)

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	app.Config.DB.Name = viper.GetString("database.name")
	app.Config.DB.Host = viper.GetString("database.host")
	app.Config.DB.Login = viper.GetString("database.login")
	app.Config.DB.Pass = viper.GetString("database.pass")
	app.Config.DB.Port = viper.GetInt("database.port")
	app.Config.Server.Port = viper.GetInt("server.port")
	app.Config.Server.Ip = viper.GetString("server.ip")

	app.Server = new(server.Server).New()
	app.Mysql = new(mysql.Mysql).New(
		fmt.Sprintf(
			"%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local",
			app.Config.DB.Login,
			app.Config.DB.Pass,
			app.Config.DB.Host,
			app.Config.DB.Port,
			app.Config.DB.Name,
		),
	)
	new(user.User).New(app.Server, app.Mysql).InitHandlers()
	new(blog.Blog).New(app.Server, app.Mysql).InitHandlers()
}

func main() {
	app.Server.Start(fmt.Sprintf("%s:%d", app.Config.Server.Ip, app.Config.Server.Port))
}
