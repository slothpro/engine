package blog

import (
	"gitlab.com/slothpro/engine/infrastructure/database/mysql"
	"gitlab.com/slothpro/engine/infrastructure/server"
)

type Blog struct {
	Server *server.Server
	DB     *mysql.Mysql
}

type Repository struct {
	Blog *Blog
}

type Service struct {
	Blog       *Blog
	Repository *Repository
}

type Handlers struct {
	Service *Service
}

var blog *Blog
var repository *Repository
var handlers *Handlers
var service *Service

func (blog *Blog) New(server *server.Server, db *mysql.Mysql) *Blog {
	blog.Server = server
	blog.DB = db

	repository = new(Repository)
	repository.Blog = blog

	service = new(Service)
	service.Repository = repository
	service.Blog = blog

	handlers = new(Handlers)
	handlers.Service = service

	blog.DB.AutoMigrate(&Post{})
	blog.DB.AutoMigrate(&Category{})

	return blog
}

func InitHandlers() {
	blog.InitHandlers()
}

func (blog *Blog) InitHandlers() *Blog {
	blog.Server.Engine.GET("/blog/post/:id", handlers.GetPost)
	blog.Server.Engine.GET("/blog/category/:id", handlers.GetCategory)

	return blog
}
