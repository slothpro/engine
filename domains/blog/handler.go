package blog

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"
)

func (h *Handlers) GetPost(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 0, 64)

	if err != nil {
		log.Panic(err)
	}

	u := h.Service.GetPostById(id)

	c.JSON(http.StatusOK, u)
}

func (h *Handlers) GetCategory(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 0, 64)

	if err != nil {
		log.Panic(err)
	}

	u := h.Service.GetCategoryById(id)

	c.JSON(http.StatusOK, u)
}
