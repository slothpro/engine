package blog

type Post struct {
	ID         int    `gorm:"AUTO_INCREMENT;primary_key"`
	Name       string `gorm:"size:255"`
	Body       string `sql:"type:text;"`
	Category   Category
	CategoryID int
}

func (Post) TableName() string {
	return "blog_posts"
}

type Category struct {
	ID   int    `gorm:"AUTO_INCREMENT;primary_key"`
	Name string `gorm:"size:255"`
}

func (Category) TableName() string {
	return "blog_categories"
}
