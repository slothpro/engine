package blog

func (r *Repository) FindPostById(id int64) *Post {
	model := new(Post)
	r.Blog.DB.First(&model, id)

	return model
}

func (r *Repository) FindPostByCategoryId(id int64) []Post {
	var models []Post
	r.Blog.DB.Where("category_id = ?", id).Find(&models)

	return models
}
