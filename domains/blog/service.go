package blog

func (s *Service) GetPostById(id int64) *Post {
	return s.Repository.FindPostById(id)
}

func (s *Service) GetCategoryById(id int64) []Post {
	return s.Repository.FindPostByCategoryId(id)
}
