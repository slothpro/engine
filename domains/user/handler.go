package user

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"
)

func (h *Handlers) GetUser(c *gin.Context) {
	id, err := strconv.ParseInt(c.Param("id"), 0, 64)

	if err != nil {
		log.Panic(err)
	}

	u := h.Service.GetById(id)

	c.JSON(http.StatusOK, u)
}
