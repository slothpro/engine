package user

type Model struct {
	ID   int    `gorm:"AUTO_INCREMENT;primary_key"`
	Name string `gorm:"size:255"`
}

func (Model) TableName() string {
	return "users"
}
