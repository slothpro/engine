package user

func (r *Repository) FindById(id int64) *Model {
	model := new(Model)
	r.User.DB.First(&model, id)

	return model
}
