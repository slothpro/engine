package user

func (s *Service) GetById(id int64) *Model {
	return s.Repository.FindById(id)
}
