package user

import (
	"gitlab.com/slothpro/engine/infrastructure/database/mysql"
	"gitlab.com/slothpro/engine/infrastructure/server"
)

type User struct {
	Server *server.Server
	DB     *mysql.Mysql
}

type Repository struct {
	User *User
}

type Service struct {
	User       *User
	Repository *Repository
}

type Handlers struct {
	Service *Service
}

var user *User
var repository *Repository
var handlers *Handlers
var service *Service

func (user *User) New(server *server.Server, db *mysql.Mysql) *User {
	user.Server = server
	user.DB = db

	repository = new(Repository)
	repository.User = user

	service = new(Service)
	service.Repository = repository
	service.User = user

	handlers = new(Handlers)
	handlers.Service = service

	user.DB.AutoMigrate(&Model{})

	return user
}

func InitHandlers() {
	user.InitHandlers()
}

func (user *User) InitHandlers() *User {
	user.Server.Engine.GET("/user/:id", handlers.GetUser)

	return user
}
