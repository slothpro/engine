package mysql

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
)

type Mysql struct {
	*gorm.DB
}

var mysql Mysql

func New(dsn string) *Mysql {
	return mysql.New(dsn)
}

func (mysql *Mysql) New(dsn string) *Mysql {
	var err error
	mysql.DB, err = gorm.Open("mysql", dsn)

	mysql.DB.LogMode(true)

	if err != nil {
		log.Fatal(err)
	}

	return mysql
}
