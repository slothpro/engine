package server

import "github.com/gin-gonic/gin"

type Server struct {
	*gin.Engine
}

var server *Server

func New() *Server {
	return server.New()
}

func (server *Server) New() *Server {
	server.Engine = gin.Default()

	return server
}

func Start(addr string) error {
	return server.Start(addr)
}

func (server *Server) Start(addr string) error {
	return server.Engine.Run(addr)
}
